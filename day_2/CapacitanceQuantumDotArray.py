# -*- coding: utf-8 -*-
"""
Created on Mon Feb  7 11:20:07 2022

@author: CharlesRW
"""
#Define setup and parameters
import numpy as np
import cvxpy as cp
import time
#from mne.viz import circular_layout
import matplotlib.pyplot as plt
from mne_connectivity.viz import plot_connectivity_circle
import copy
import scipy

class CapacitanceQuantumDotArray():
    '''
    Object defining a quantum dot array in the constant-capacitance model.
    For specification details, see __init__ below.
    '''

    # charge of electron - set to 1 for eV
    # Mainly this way for numerical reasons; I believe other values should
    # work if appropriate care is taken numerically, but I have not tested
    # this and do not guarantee it
    unitCharge = 1.

    def __init__(self, dotDotMutualCapacitanceMatrix, gateDotMutualCapacitanceMatrix, physicalDotLocations, dotAdjacencyMatrix, whichDotsAreOhmicConnected):
        '''
            ARGUMENTS

                dotDotMutualCapacitanceMatrix : 2D array of (non-negative) floats, shape = numDots x numDots
                    element i,j of this matrix is the capacitance between dot i and dot j
                    NB this should NOT be confused with the maxwell capacitance matrix between the dots

                gateDotMutualCapacitanceMatrix : 2D array of non-negative floats, shape = numDots x numGates
                    element i,j of this matrix is the mutual capacitance between dot i and gate j
                    NB this also becomes part of the system maxwell matrix

                physicalDotLocations : list of 2-tuples; there are numDots such 2-tuples
                    Each entry in this list is the (x,y) co-ordinates of the corresponding dot.
                    NB this is used ONLY for potential-sensing (which needs to calculate a distance vector)
                    - it is NOT used for capacitance calculations nor for determining if dots are 'adjacent'.

                dotAdjacencyMatrix : 2D array of {0,1}, shape = numDots x numDots ; must be symmetric
                    elemenent i,j of this matrix is 1 if dot i and dot j are connected, else 0.
                    Indicates which dots are 'connected'; used only in the dynamical 
                    part of the simulator.  Dots which are connected allow electrons to move between them.

                whichDotsAreOhmicConnected : 1D array of ints
                    subset of dot indices (from 0 up to numDots-1 inclusive).  Used only for the dynamical simulator.
                    If a dot's index is in this list, it is 'connected to an ohmic contact', which operationally means
                    that we admit transitions where electrons flow in or out of this dot from a reservoir
                    
        '''

        # Total number of dots in the array
        self.numDots = np.shape(dotDotMutualCapacitanceMatrix)[0]

        # Tests to ensure dimensions are as assumed and matrices symmetric where required
        assert np.shape(dotDotMutualCapacitanceMatrix) == (self.numDots,self.numDots)
        assert np.all(dotDotMutualCapacitanceMatrix == dotDotMutualCapacitanceMatrix.T)
        assert np.all(dotDotMutualCapacitanceMatrix >= 0)
        assert np.all(gateDotMutualCapacitanceMatrix >= 0)
        if not dotAdjacencyMatrix is None:
            assert np.all((dotAdjacencyMatrix == 0) + (dotAdjacencyMatrix == 1))
            assert np.all(dotAdjacencyMatrix == dotAdjacencyMatrix.T)

        # Number of uniqe gate voltages
        self.numGates = np.shape(gateDotMutualCapacitanceMatrix)[1]

        self.gateDotMutualCapacitanceMatrix = gateDotMutualCapacitanceMatrix
        self.dotDotMutualCapacitanceMatrix = dotDotMutualCapacitanceMatrix

        # Calculate the maxwell capacitance matrix
        # C = [[C_DD, C_DG^T], [C_DG, C_GG]]

        # Dx(D+G) = DxD + DxG, ax=1
        tmpLeft = np.concatenate( (dotDotMutualCapacitanceMatrix, gateDotMutualCapacitanceMatrix), axis=1)

        # Gx(D+G) = GxD + GxG, ax = 1
        # NB here, the second matrix is the gate-gate capacitance matrix
        tmpRight = np.concatenate((gateDotMutualCapacitanceMatrix.T, np.identity(self.numGates)), axis=1)

        #(D+G)x(D+G) = Dx(D+G) + Gx(D+G) , ax = 0
        self.systemMutualCapacitanceMatrix = np.concatenate((tmpLeft,tmpRight),axis=0)

        # check dimensions
        assert np.shape(self.systemMutualCapacitanceMatrix) == (self.numDots + self.numGates, self.numDots + self.numGates)

        # finally, calculate the Maxwell matrix for hte whole system
        self.systemMaxwellMatrix = self.mutualToMaxwell(self.systemMutualCapacitanceMatrix)

        # NB per problem formulation, charging-energy matrix is C_DD^-1, BUT
        # this nonetheless includes the dot-gate capacitances, as they go to
        # the diagonal entries; effectively increasing self-capacitance

        # Get dot-dot portion of the block matrix
        dotDotMaxwellMatrix = self.systemMaxwellMatrix[:self.numDots, :self.numDots]
        self.inverseDotDotCapacitanceMatrix = np.linalg.inv(dotDotMaxwellMatrix)
        # this is the E_C matrix
        #print(self.inverseDotDotCapacitanceMatrix)

        self.gateDotMaxwellMatrix = self.systemMaxwellMatrix[ :self.numDots, self.numDots: ]
        assert np.shape(self.gateDotMaxwellMatrix) == (self.numDots,self.numGates)

        # The Potential equation is:
        # U(N)= 1/2 * (e^2 * N*Ec*N - 2*e * (CV)*Ec*N)
        # Write as U(N) = 0.5 * N*A*N + b*N -->
        # A = Ec *e^2
        # b = -e (C_DG @ V_G)^T @ Ec

        # NB we do NOT store the 0.5 as part of the A expression
        self.A = self.inverseDotDotCapacitanceMatrix * (self.unitCharge**2)
        self.b = cp.Parameter(self.numDots)
        self.x = cp.Variable(self.numDots, integer = True)
        self.energyExpr = 0.5*(cp.quad_form(self.x,self.A)) + self.b.T @ self.x
        self.groundStateProblem = cp.Problem(cp.Minimize(self.energyExpr),[self.x>=0])


        #Store for later use
        self.physicalDotLocations = np.array(physicalDotLocations)
        # Store dot adjacency matrix for later use
        self.dotAdjacencyMatrix = dotAdjacencyMatrix
        #Likewise for ohmic connections
        self.whichDotsAreOhmicConnected = whichDotsAreOhmicConnected

    def mutualToMaxwell(self, systemMutualCapacitanceMatrix):
        '''
        Converts mutual capacitances to maxwell capacitance matrix
        maxwell capacitance matrix is the one satisfying Q=CV

        ARGUMENTs

            systemMutualCapacitanceMatrix : 2D array of floats
                Entry i,j of this matrix is the mutual capacitance 
                between object i and object j. Dot-dot, dot-gate, 
                and gate-gate interactions are included
        
        RETURNS

            systemMaxwellMatrix : 2D array of (some negative) floats
                The Maxwell capacitance matrix corresponding 
                to the mutual capacitances given
        '''
        systemMaxwellMatrix = -1.0*systemMutualCapacitanceMatrix
        for lv in range(len(systemMutualCapacitanceMatrix)):
            systemMaxwellMatrix[lv,lv] = np.sum( systemMutualCapacitanceMatrix[:,lv] )
        return systemMaxwellMatrix

    def findGroundState(self, gateVoltages):
        '''
            Function which determines the dot occupations in the ground state.
            This is done by solving a convex optimisation problem
            N.B. The occupations returned are NOT in general a unique solution
                but the energy is guaranteed minimal.
           
            ARGUMENTS

                gateVoltages : 1D array of floats, shape = numGates
                    The voltage at which each gate is set

            RETURNS

                groundStateOccupations : 1D array of ints, shape = numDots
                    Occupation of each dot (i.e. the problem solution)

                groundStateEnergy : float
                    Value of optimisation problem ( = energy up to scale factor)

                wallTime : float
                    wall time taken by the convex optimisation problem 
                    (does not include time taken for any other part of the procedure)
        '''
        # Dot occupations == decision variable


        # affine term of optimisation, changes bsaed on gateVoltages
        # --> should not be stored with object, as it changes during each run
        self.b.value = - self.unitCharge * (self.gateDotMaxwellMatrix @ gateVoltages) @ self.inverseDotDotCapacitanceMatrix

        tic = time.time()
        self.groundStateProblem.solve(solver='MOSEK', warm_start=True)#,verbose=True)# mosek_params=mosek_params)
        #groundStateProblem.solve(solver='SCIP', warm_start=True,verbose=True)# mosek_params=mosek_params)
        toc = time.time()
        wallTime = toc-tic
        #print("[solver] done one",flush=True)
        assert self.groundStateProblem.status == 'optimal'

        groundStateOccupations = self.x.value
        assert np.all([abs(a - np.rint(a))<1e-4 for a in groundStateOccupations])
        groundStateOccupations = [np.rint(a) for a in groundStateOccupations]
        groundStateEnergy = self.groundStateProblem.value

        return groundStateOccupations, groundStateEnergy, wallTime

    def getAllSingleElectronTransitions(self,currentOccupations):
        '''
            For a given occupation state, returns a list of all possible
            single-electron transitions - i.e. a transition in which a single
            electron enters/leaves the system, or hops from one dot to a neighbor.
            NB does NOT allow negative electron numbers (holes)
            
            ARGUMENTS

                currentOccupations : list of ints of length numDots
                    Occupation state from which to calculate allowed 
                    occupations (used only to ensure that transitions do not
                    result in negative occupations)

            RETURNS

                transitions : list of lists (inner list contains integers) ; inner list length = numDots;
                outer list length (=number of allowed transitions) depends on setup
                    List of transitions.  One transition is represented as an array each entry of which has 
                    +/- 1 for dots which gain/lose electrons, otherwise zero.
        '''
        transitions = []

        # Consider the possibility of no-transition
        transitions += [[0]*self.numDots]

        # Then all ohmic contacts add/subtract one e-:
        for lvDot in self.whichDotsAreOhmicConnected:
            #dotIdx = lvX*self.numDotsSide + lvY

            # electron flows into this dot
            transitions += [[+1 if lv == lvDot else 0 for lv in range(self.numDots)]]

            # electron flows out of this dot
            transitions += [[-1 if (lv == lvDot and currentOccupations[lvDot]>0) else 0 for lv in range(self.numDots)]]

        # Now go through all pairs of neighbors
        for lv1 in range(self.numDots):
            for lv2 in range(self.numDots):
                # obviously skip the case where its the same dot
                if lv1 == lv2:
                    continue
                if self.dotAdjacencyMatrix[lv1,lv2] == 1:
                    # only add the +1, -1 transition here; since the matrix is symmetric
                    # the -1, +1 one will be added later in the loop
                    transitions += [[+1 if lv3 == lv1 else -1 if lv3 == lv2 and currentOccupations[lv2]>0 else 0 for lv3 in range(self.numDots)]]

        # check that I didn't do the indexing wrong
        for t in transitions:
            assert len(t) == self.numDots

        return transitions

    def calculateEnergy(self, currentOccupations, gateVoltages):
        '''
        Function which calculates energy in a particular configuration.

        ARGUMENTS

            currentOccupations : 1D array of ints, shape = numDots
                Occupation of each dot (i.e. the problem solution)

            gateVoltages : 1D array of floats, shape = numGates
                The voltage at which each gate is set

        RETURNS

            energy : float
                electrostatic energy of the given configuration 
                (NB this neglects the same terms as the other energy functional,
                 and for the same reason)
        '''
        quadraticTerm = 0.5 * currentOccupations.T @ self.inverseDotDotCapacitanceMatrix @ currentOccupations * (self.unitCharge**2)
        affineTerm = - self.unitCharge * ((self.gateDotMaxwellMatrix @ gateVoltages).T) @ self.inverseDotDotCapacitanceMatrix @ currentOccupations

        assert isinstance(quadraticTerm,float)
        assert isinstance(affineTerm,float)

        energy = quadraticTerm + affineTerm
        return energy


    def dynamicsOneStep(self, currentOccupation, gateVoltages):
        '''
        Function which determines which single-electron transitions
        result in lowest energy

        ARGUMENTS

            currentOccupations : 1D array of ints, shape = numDots
                Occupation of each dot (i.e. the problem solution)

            gateVoltages : 1D array of floats, shape = numGates
                The voltage at which each gate is set

        RETURNS

            acceptableOccupations : 1D array of ints, len=numDots
                list of all occupation states (which can be reached from
                the present one by an admissible transition) which yield the 
                minimum energy

            lowestEnergy : float
                energy of the lowest accessible configurations
        '''

        transitions = self.getAllSingleElectronTransitions(currentOccupation)
        possibleOccupations = [np.array(list(np.array(t) + np.array(currentOccupation))) for t in transitions]
        energyArr = []

        #for the occupation after each transition
        for lv,occupation in enumerate(possibleOccupations):
            #calculate the energy and store it
            energy = self.calculateEnergy(occupation,gateVoltages)
            energyArr += [energy]

        #look at all states which have the lowest minimum energy
        lowestEnergy = min(energyArr)

        # index of all states with ~same energy
        idxLowestEnergies = []
        for lv in range(len(possibleOccupations)):
            if abs(energyArr[lv] - lowestEnergy) < 1e-6:
                idxLowestEnergies += [lv]

        acceptableOccupations = [possibleOccupations[lv] for lv in idxLowestEnergies]

        return acceptableOccupations, lowestEnergy

    def dynamicsEquilibrate(self, initialOccupations, gateVoltages):
        '''
        Repeatedly takes the single-electron transition which goes 
        to the lowest energy.

        ARGUMENTS

            currentOccupations : 1D array of ints, shape = numDots
                Occupation of each dot (i.e. the problem solution)

            gateVoltages : 1D array of floats, shape = numGates
                The voltage at which each gate is set

        RETURNS
            
            energySeq : list of floats
                List containing the energy of the system at the beginning of each timestep

            occupationSeq : list of occupation states (occupation states are list of ints of length numDots)
                List containing the occupation-state of the system at the beginning of each timestep
        '''

        currentEnergy = self.calculateEnergy(initialOccupations,gateVoltages)
        currentOccupations = initialOccupations

        energySeq = []
        occupationsSeq = []

        deltaEnergy = -1

        #until the energy stops decreasing
        while deltaEnergy < 0 :
            energySeq += [currentEnergy]
            occupationsSeq += [currentOccupations]

            acceptableNextOccupations, nextEnergy = self.dynamicsOneStep(currentOccupations,gateVoltages)

            #if there are iso-energy states, I'd like to know
            if len(acceptableNextOccupations) > 1:
                pass
                #print("[SquareCapacitanceQuantumDotArray :: dynamicsEquilibrate :: WARNING] Found iso-energy states, choosing one at random")

            #in case multiple occupations are iso-energy, choose one at random
            nextOccupations = acceptableNextOccupations[np.random.randint(low=0,high=len(acceptableNextOccupations))]
            nextOccupations = np.array(nextOccupations)

            deltaEnergy = nextEnergy - currentEnergy
            assert deltaEnergy <= 0

            #update
            currentEnergy = nextEnergy
            currentOccupations = nextOccupations

        # return the time history
        return energySeq, occupationsSeq

    def sensePotential(self, currentOccupation, sensorCoordinates):
        '''
        Function which calculates the potential sensed by 
        a charge-sensor at location sensorCoordinates, given the specified occupation
        NB this 'potential' is not physical, failing to include any effects from the gates
        it is meant only to illustrate changes in occupation for charge-stability diagrams
        NB Units are arbitrary

        ARGUMENTS

            currentOccupations : 1D array of ints, shape = numDots
                Occupation of each dot (i.e. the problem solution)

            sensorCoordinates : 2-tuple of floats
                (x,y) co-ordinates at which the potential is to be measured.
                Used to calculate the radius vector from each dot

        RETURNS

            potential : float
                Value of the electrostatic potential sensed
        '''
        #initialise at 0
        potential = 0

        #sensor location
        xSensor, ySensor = sensorCoordinates

        #For each dot
        for lvDot in range(self.numDots):
            xDot, yDot = self.physicalDotLocations[lvDot]
            dX = xSensor - xDot
            dY = ySensor - yDot
            R = np.sqrt(dX**2 + dY**2)
            assert R > 1e-10 #ensure sensor isn't on any dots

            potential += currentOccupation[lvDot]/R

        return potential

    def probeManyVoltagesInARow(self, voltageArr):
        '''
        Function designed to facilitate the probing of many voltage points at once

        ARGUMENTS

            voltageArr : list of 1D arrays, each list is a gateVoltage specification (of length numGates)
                Specification of which voltage-space points to sample.
                Different probe-points are specified by the first (outer-list) index.
                Inner-list index specifies the voltage for a particluar gate
                We call the total number of points given to measure at as numProbePoints

        RETURNS

            occupationArr : 2D array of ints, size is numProbePoints x numDots
                Stores the occupation-state of the array for each given voltage.
                First index is for which voltage point
                Second index is for which dot

            energyArr : 1D array of floats, size is numProbePoints
                Each entry is the energy of the ground state for the corresponding voltage-point
        '''
        numProbePoints = np.shape(voltageArr)[0]

        # intiialise arrays for storing results
        occupationArr = np.zeros( shape=(numProbePoints, self.numDots) )
        energyArr = np.zeros( shape=(numProbePoints) )

        for lv, voltage in enumerate(voltageArr):
            occupation, energy, _ = self.findGroundState(voltage)
            occupationArr[lv, :] = occupation
            energyArr[lv] = energy
            #print("Done one")

        return occupationArr, energyArr

    def capacitanceDiagram(self,title="",hasColorbar=False, hasLabels=False):
        '''
        Returns two connectivity-circle figures.  In this figure, dots and gates are placed in a circle,
        with the thickness of lines between them set according to their mutual capacitance.
        The first plot shows gate-dot connection, while the second shows dot-dot.
        Commented code displays analogous diagram for the whole system.
        Variable names replicate those found in plotting function documentation:
        https://www.nmr.mgh.harvard.edu/mne/0.14/generated/mne.viz.plot_connectivity_circle.html
        More detailed changes to the plot can be made by altering the figure code below.

        ARGUMENTS:

            title : string
                Title of the plot

            hasColorbar : Boolean
                True puts colorbar on plot, False doesn't

            hasLabels : Boolean
                True labels dots and gates as, e.g. "D0" "G0", etc.  False suppresses labels

        RETURNS:

            fig2,ax2,fig3,ax3 : two figure-axis pairs
                fig2 and ax2 correspond to the dot-gate capacitance diagram.
                fig3 and ax3 correspond to the dot-dot diagram.
        '''

        #Normalise wrt largest system capacitance
        normalisation = max(np.max(self.dotDotMutualCapacitanceMatrix),np.max(self.gateDotMutualCapacitanceMatrix))


        # Define plot parameters to be used for gate-dot diagram

        #con = self.systemMutualCapacitanceMatrix / normalisation
        gateStartAngle = - 360./8.
        gateStopAngle = + 360 / 8
        gateAngleWidth = (gateStopAngle - gateStartAngle)/self.numGates
        gateAngles = np.linspace(gateStartAngle + gateAngleWidth/2, gateStopAngle - gateAngleWidth/2,self.numGates)

        dotStartAngle = +90.
        dotStopAngle = +270.
        dotAngleWidth = (dotStopAngle-dotStartAngle)/self.numDots
        dotAngles = np.linspace(dotStartAngle + dotAngleWidth/2, dotStopAngle - dotAngleWidth/2,self.numDots)

        node_angles = np.array([*list(dotAngles),*list(gateAngles)])
        node_angles = node_angles + 90

        node_colors = ["red"] * self.numDots + ["green"] * self.numGates
        node_names = ["D"+str(lv) for lv in range(self.numDots)] + ["G"+str(lv) for lv in range(self.numGates)]
        if not hasLabels:
            node_names = [""]*(self.numDots + self.numGates)

        node_width = min(gateAngleWidth, dotAngleWidth)
        
        colorbar=hasColorbar

        facecolor="white"
        textcolor="black"
        
        fontsize_title=20
        fontsize_names=20
        fontsize_colorbar=20

        colorbar_size=0.5
        colorbar_pos= (0, .5)
        linewidth=2.0

        n_lines = 100
        '''
        fig1,ax1 = plot_connectivity_circle(con, node_names,
                                          colorbar=hasColorbar,
                                          node_angles=node_angles,
                                          node_colors=node_colors,
                                          colormap='binary',
                                          node_width=node_width,
                                          facecolor=facecolor,
                                          textcolor=textcolor,
                                          title="", #+ " (Whole System)",
                                          fontsize_title=fontsize_title,
                                          fontsize_names=fontsize_names,
                                          fontsize_colorbar=fontsize_colorbar,
                                          colorbar_size=colorbar_size,
                                          colorbar_pos=colorbar_pos,
                                          linewidth=linewidth,
                                          n_lines=n_lines
                                          )
        '''
        #plt.savefig(title + "sysCapDiagram.png",format="png")

        # One with just gate-dot, since everything is the same
        #con = self.gateDotMaxwellMatrix

        # Gate-dot capacitance figure:

        tmp = copy.copy(self.systemMutualCapacitanceMatrix)
        tmp[:self.numDots,:self.numDots] = np.zeros(shape=(self.numDots,self.numDots))
        tmp[self.numDots:,self.numDots:] = np.zeros(shape=(self.numGates,self.numGates))
        con = tmp / normalisation
        n_lines = 64
        fig2,ax2 = plot_connectivity_circle(con,
                                          node_names,
                                          colorbar=hasColorbar,
                                          node_angles=node_angles,
                                          node_colors=node_colors,
                                          colormap='binary',
                                          node_width=node_width,
                                          facecolor=facecolor,
                                          textcolor=textcolor,
                                          title= "Gate-Dot Mutual Capacitances (Normalised)",
                                          fontsize_title=fontsize_title,
                                          fontsize_names=fontsize_names,
                                          fontsize_colorbar=fontsize_colorbar,
                                          colorbar_size=colorbar_size,
                                          colorbar_pos=colorbar_pos,
                                          linewidth=linewidth,
                                          n_lines=n_lines
                                          )

        # Dot-dot capacitance figure
        node_angles = np.array([lv/self.numDots*360 for lv in range(self.numDots)])
        node_angles = node_angles + 90


        node_colors = ["red"] * self.numDots
        node_names = node_names[:self.numDots]

        con = self.dotDotMutualCapacitanceMatrix
        n_lines = 256
        fig3,ax3 = plot_connectivity_circle(con,
                                          node_names,
                                          colorbar=hasColorbar,
                                          node_angles=node_angles,
                                          node_colors=node_colors,
                                          colormap='binary',
                                          node_width=node_width,
                                          facecolor=facecolor,
                                          textcolor=textcolor,
                                          title="Dot-Dot Mutual Capacitances (Normalised)",
                                          fontsize_title=fontsize_title,
                                          fontsize_names=fontsize_names,
                                          fontsize_colorbar=fontsize_colorbar,
                                          colorbar_size=colorbar_size,
                                          colorbar_pos=colorbar_pos,
                                          linewidth=linewidth,
                                          n_lines=n_lines
                                          )
        return fig2, ax2, fig3, ax3 #fig1, ax1,

    def drawPhysicalArray(self, sensorLocations,dotLabels=False,sensorLabels=False):
        '''
        Function to plot an illustration of the physical setup of dots 
        and charge-sensors.  May require manual tampering with the parameters
        below for adequate results.  Does NOT plot gates, which, if desired, 
        must be added _ex post_.

        ARGUMENTS
            
            sensorLocations : 2D numpy array shape = (numSensors,2)
                Array containing coordinates of all sensors

            dotLabels : Boolean
                True labels dots as, e.g. "D0" False suppresses these labels

            sensorLabels : Boolean
                True labels sensors as, e.g. "S0"  False suppresses these labels

        RETURNS

            fig : matplotlib figure
                The figure handle for the figure generated
        '''
        fig = plt.figure()
        sensorLocations = np.array(sensorLocations)
        assert self.physicalDotLocations is not None
        xLeftDots = np.min(self.physicalDotLocations[:,0])
        xRightDots = np.max(self.physicalDotLocations[:,0])

        yBotDots = np.min(self.physicalDotLocations[:,1])
        yTopDots = np.max(self.physicalDotLocations[:,1])

        for lv in range(self.numDots):
            legend = "Dot" if lv == 0 else "_nolegend_"
            if dotLabels:
                txt = "D" + str(lv)
                plt.annotate(txt, self.physicalDotLocations[lv,:])
            else:
                txt = ""
            plt.scatter(*self.physicalDotLocations[lv],marker='o',label=legend,color="red")
        #legend = ["Dot"]

        #Now for sensors as well
        if not sensorLocations is None:
            xLeftSensors = np.min(sensorLocations[:,0])
            xRightSensors = np.max(sensorLocations[:,0])

            yBotSensors = np.min(sensorLocations[:,1])
            yTopSensors = np.max(sensorLocations[:,1])
            for lv, _ in enumerate(sensorLocations):
                legend = "Sensor" if lv == 0 else "_nolegend_"
                if sensorLabels:
                    txt = "S" + str(lv)
                    plt.annotate(txt, sensorLocations[lv,:])
                else:
                    txt = ""

                plt.scatter(*sensorLocations[lv],marker='v',label=legend,color="green")
            xLeft = min(xLeftDots,xLeftSensors)
            xRight = max(xRightDots,xRightSensors)
            yBot = min(yBotDots,yBotSensors)
            yTop = max(yTopDots,yTopSensors)

            #legend += ["Charge Sensor"]
        else:
            xLeft = xLeftDots
            xRight = xRightDots
            yBot = yTopDots
            yTop = yBotDots

        width = xRight - xLeft
        height = yTop - yBot

        widthScale=1.5
        heightScale=1.5

        xLeft -= (widthScale-1)*width
        #xLeft += 6.0
        xRight +=  (widthScale-1)*width
        #xRight -=1.5
        yBot -= (heightScale-1)*height
        #yBot += 6.0
        yTop += (heightScale-1)*height
        #yTop += 1.5

        plt.rc('font',size=15)
        plt.rc('axes',titlesize=15)
        #plt.rc('axes',titlesize=15)
        plt.xlim([xLeft,xRight])
        plt.ylim([yBot,yTop])
        plt.xlabel("X Location (Arbitrary Units)")
        plt.ylabel("Y Location (Arbitrary Units)")
        plt.legend()


        return fig

    def findBasisVectors(self):
        '''
        This function calculates bases for the different systems which define the system.
        N.B. Integrality of the eta and Upsilon vectors are not enforced, nor is
        orthonormality of eta and Upsilon with respect to the E_C-induced norm.
        I.e. eta^T_i eta_j = Upsilon^T_i Upsilon_j = delta_ij
        but eta^T_i Upsilon_j will NOT equal delta_ij, nor will eta^T_i E_C  Upsilon_j

        NO ARGUMENTS

        RETURNS

            upsilonBasis : array of shape numDots x (numDots - numGates)
                Array of vectors which span the space of zero-energy transitions.
                Defined as spanning Null(C_G^T E_C)
                Second index selects between different vectors
                First index selects component of such a vector

            etaBasis : array of shape numDots x numGates
                Array of gate-voltage induced transitions, defined as an 
                orthonormal basis of Range(C)
                Second index selects between different vectors
                First index selects component of such a vector

            deltaVBasis : array of shape numGates x numGates
                Array of vectors generating the corresponding eta vectors
                Defined by C deltaV_i = eta_i
                Second index selects between different vectors
                First index selects component of such a vector
        '''

        # Transitions which can't be achieved by changing V
        #nullCT = scipy.linalg.null_space(C.T)

        # Transitions which can be achieved - eta_i ~ Range(C)
        etaBasis = scipy.linalg.orth(self.gateDotMaxwellMatrix)
        deltaVBasis,_,_,_ = np.linalg.lstsq(self.gateDotMaxwellMatrix, etaBasis)

        #Transitions which are impossible: Null(C.T @ E_C)
        upsilonBasis = scipy.linalg.null_space(self.gateDotMaxwellMatrix.T @ self.inverseDotDotCapacitanceMatrix)

        return upsilonBasis, etaBasis, deltaVBasis